#!/bin/bash
shopt -s extglob

if [[ -e "$HOME/git/dracut/$1" ]]; then
    srcrpm="$HOME/git/dracut/$1"
elif [[ -e "$HOME/dev/upstream/dracut/$1" ]]; then
    srcrpm="$HOME/dev/upstream/dracut/$1"
else
    srcrpm="$1"
fi

[[ -f $srcrpm ]] || exit 0

old_release=$(rpmspec -D "_sourcedir $(pwd)" -q --srpm --qf '%{release}' dracut.spec)
old_release=${old_release%%.*}

cp dracut.spec dracut.spec.old
for i in *.patch; do git rm -f $i;done

if rpm -ivh --define "_srcrpmdir $PWD" --define "_specdir $PWD" --define "_sourcedir $PWD" "$srcrpm"; then
	ls *.patch &>/dev/null && git add *.patch

    new_version=$(rpmspec -D "_sourcedir $(pwd)" -q --srpm --qf '%{version}' dracut.spec)
    new_release=$(rpmspec -D "_sourcedir $(pwd)" -q --srpm --qf '%{release}' dracut.spec)
    new_release_full=${new_release%.*}
    new_release=${new_release%%.*}

    do_print=""
    while IFS=$'\n' read -r line
    do
        if [ -z "$do_print" ] && [ "$line" = "%changelog" ]; then
            do_print="yes"
            echo "* $(LANG='C' date '+%a %b %d %Y') $(git config user.name) <$(git config user.email)> - ${new_version}-${new_release_full}"

            for ((i=old_release; i<new_release; i++)); do
                subject=$(grep '^Subject: ' +(0)$i.patch | head -1)
                if [ -n "$subject" ]; then
                    echo "-${subject#*\[PATCH\]}"
                fi
            done

            echo

        elif [ -n "$do_print" ]; then
            echo "$line"
        fi
    done < dracut.spec.old >> dracut.spec

    git add dracut.spec

    msg="Resolves: $(
    for ((i=old_release; i<new_release; i++)); do
        resolves=$(grep '^Resolves: ' +(0)$i.patch | head -1)
        if [ -n "$resolves" ]; then
            echo "${resolves#Resolves: }"
        fi
    done | sort -u | tr '\n' ',')"
    git commit -m "dracut-${new_version}-${new_release_full}

${msg%,}"

fi
